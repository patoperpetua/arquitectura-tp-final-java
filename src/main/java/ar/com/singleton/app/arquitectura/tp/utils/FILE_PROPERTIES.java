/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.app.arquitectura.tp.utils;

/**
 *
 * @author pato
 */
public class FILE_PROPERTIES {
    public static final String ARG_FILE_NAME = "main.properties";
    public static final String ARG_PORT = "PORT";
    public static final String ARG_SKIN = "SKIN"; 
    public static final String ARG_BAUD = "BAUD";
    public static final String ARG_COMMENTS = "File properties for Arquitectura Final TP";
}
