/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.app.arquitectura.tp.tables;


import ar.com.singleton.app.arquitectura.tp.models.Memories;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Pato
 */
public class ModelTableMemory extends AbstractTableModel{

    private List<Memories> li = new ArrayList();
    private final String[] columnNames = {"Referencia","HEXA","DEC","Binary"};
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Memories oAlquiler = li.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return oAlquiler.getReference();
            case 1:
                return oAlquiler.getHexa();
            case 2:
                return oAlquiler.getValue();
            case 3:
                return oAlquiler.getBinary();
            }
        return null;
   }

   @Override
   public Class<?> getColumnClass(int columnIndex){
        switch (columnIndex){
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return Integer.class;
            case 3:
                return String.class;
        }
        return null;
    }
   
    public ModelTableMemory(List<Memories> list){
        this.li = list;
    }

    @Override
    public String getColumnName(int columnIndex){
         return columnNames[columnIndex];
    }

    @Override     
    public int getRowCount() {
        return li.size();
    }

    @Override        
    public int getColumnCount() {
        return columnNames.length; 
    }
    
    @Override
    public boolean isCellEditable(int i, int i1) {
        return false;
    }
 }