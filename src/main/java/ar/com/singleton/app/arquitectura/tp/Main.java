/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.app.arquitectura.tp;

import ar.com.singleton.app.arquitectura.tp.utils.FILE_PROPERTIES;
import ar.com.singleton.app.arquitectura.tp.views.MainView;
import ar.com.singleton.app.arquitectura.tp.views.ViewConfiguration;
import com.fazecast.jSerialComm.SerialPort;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import javax.swing.SwingUtilities;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.pushingpixels.substance.api.skin.SaharaSkin;

/**
 *
 * @author pato
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        File oFile = new File(FILE_PROPERTIES.ARG_FILE_NAME);
        Properties fileProperties = new Properties();
        String skin = "org.pushingpixels.substance.api.skin.SaharaSkin";
        String serialPort = null;
        if(oFile.exists()){
            fileProperties.load(new FileInputStream(oFile));
            serialPort = fileProperties.getProperty(FILE_PROPERTIES.ARG_PORT);
            skin = fileProperties.getProperty(FILE_PROPERTIES.ARG_SKIN,skin);
        }else{
            fileProperties.setProperty(FILE_PROPERTIES.ARG_SKIN, skin);
            fileProperties.store(new FileOutputStream(oFile), "File properties for Arquitectura Final TP");
            
        }
        
        setSkin(skin);
        
        if(serialPort == null)
            ViewConfiguration.main(null);
        else{
            SerialPort[] ports = SerialPort.getCommPorts();
            for(SerialPort port: ports){
                if(port.getSystemPortName().equals(serialPort)){
                    MainView.main(port);
                    return;
                }
            }
            ViewConfiguration.main(null);
        }
    }
    
    public static void setSkin(String skin){
        SwingUtilities.invokeLater(() -> {
            SubstanceLookAndFeel.setSkin(skin);
        });
    }
}
