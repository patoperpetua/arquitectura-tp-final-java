/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.app.arquitectura.tp.models;

/**
 *
 * @author pato
 */
public enum ProcessorStates {
    UNKNOWN,
    IDLE,
    PROGRAMMING,
    STEP_BY_SETEP,
    WAITING,
    ERROR;
}
