/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.app.arquitectura.tp.models;

/**
 *
 * @author pato
 */
public class Signals {
    public static final int SIG_START = 1;
    public static final int SIG_CONTINUOUS = 2;
    public static final int SIG_STEP_BY_STEP = 4;
    public static final int SIG_RE_PROGRAM = 8;
    public static final int SIG_NEXT_STEP = 16;
}
