/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.app.arquitectura.tp.models;

import java.util.Objects;

/**
 *
 * @author pato
 */
public class Instructions {
    private Integer opcode;
    private Integer rd;
    private Integer rt;
    private Integer rs;
    private Integer sa;
    private Integer offset;
    private Integer address;
    private Integer funct;
    
    private String line;
    private String comments;
    private String meaning;

    public Instructions() {
    }

    public Instructions(Integer opcode, Integer address) {
        this.opcode = opcode;
        this.address = address;
    }
    
    public Integer getOpcode() {
        return opcode;
    }

    public void setOpcode(Integer opcode) {
        this.opcode = opcode;
    }

    public Integer getRd() {
        return rd;
    }

    public void setRd(Integer rd) {
        this.rd = rd;
    }

    public Integer getRt() {
        return rt;
    }

    public void setRt(Integer rt) {
        this.rt = rt;
    }

    public Integer getRs() {
        return rs;
    }

    public void setRs(Integer rs) {
        this.rs = rs;
    }

    public Integer getSa() {
        return sa;
    }

    public void setSa(Integer sa) {
        this.sa = sa;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getAddress() {
        return address;
    }

    public void setAddress(Integer address) {
        this.address = address;
    }

    public Integer getFunct() {
        return funct;
    }

    public void setFunct(Integer funct) {
        this.funct = funct;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }
    
    public String getBinary(){
        StringBuilder builder = new StringBuilder();
        builder.append(Integer.toBinaryString( 64 | opcode).substring(1));
        switch(opcode){
            case 0:
                switch(funct){
                    case 0:
                    case 2:
                    case 3:
                        builder.append("00000");
                        builder.append(Integer.toBinaryString( 32 | rt).substring(1));
                        builder.append(Integer.toBinaryString( 32 | rd).substring(1));
                        builder.append(Integer.toBinaryString( 32 | sa).substring(1));
                        builder.append(Integer.toBinaryString( 64 | funct).substring(1));
                        break;
                    case 4:
                    case 6:
                    case 7:
                    case 33:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 42:
                        builder.append(Integer.toBinaryString( 32 | rs).substring(1));
                        builder.append(Integer.toBinaryString( 32 | rt).substring(1));
                        builder.append(Integer.toBinaryString( 32 | rd).substring(1));
                        builder.append("00000");
                        builder.append(Integer.toBinaryString( 64 | funct).substring(1));
                        break;
                    case 8:
                        builder.append(Integer.toBinaryString( 32 | rs).substring(1));
                        builder.append("000000000000000");
                        builder.append(Integer.toBinaryString( 64 | funct).substring(1));
                        break;
                    case 9:
                        builder.append(Integer.toBinaryString( 32 | rs).substring(1));
                        builder.append("00000");
                        builder.append(Integer.toBinaryString( 32 | rd).substring(1));
                        builder.append("00000");
                        builder.append(Integer.toBinaryString( 64 | funct).substring(1));
                        break;
                    default:
                        return null;
                }
                break;
            case 4:
            case 5:
            case 10:    
            case 8:
            case 12:
            case 13:
            case 14:
            case 32:
            case 33:
            case 35:
            case 36:
            case 37:
            case 39:
            case 40:
            case 41:
            case 43:
                builder.append(Integer.toBinaryString( 32 | rs).substring(1));
                builder.append(Integer.toBinaryString( 32 | rt).substring(1));
                String off = Integer.toBinaryString(offset);
                for (int i = 0; i < 16-off.length(); i++) {
                    builder.append("0");
                }
                builder.append(off);
                break;
            case 15:
                builder.append("00000");
                builder.append(Integer.toBinaryString( 32 | rt).substring(1));
                off = Integer.toBinaryString(offset);
                for (int i = 0; i < 16-off.length(); i++) {
                    builder.append("0");
                }
                builder.append(off);
                break;
            case 2:
            case 3:
                off = Integer.toBinaryString(address);
                for (int i = 0; i < 26-off.length(); i++) {
                    builder.append("0");
                }
                builder.append(off);
                break;
            case 63:
                builder.append(Integer.toBinaryString(address));
                break;
            default:
                return null;
        }
        return builder.toString();
    }
    
    public String getInstruction(){
        StringBuilder builder = new StringBuilder();
        switch(opcode){
            case 0:
                switch(funct){
                    case 0:
                        builder.append("SLL");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rt);
                        builder.append(" ");
                        builder.append(sa);
                        break;
                    case 2:
                        builder.append("SRL");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rt);
                        builder.append(" ");
                        builder.append(sa);
                        break;
                    case 3:
                        builder.append("SRA");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rt);
                        builder.append(" ");
                        builder.append(sa);
                        break;
                    case 4:
                        builder.append("SLLV");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rt);
                        builder.append(" ");
                        builder.append(rs);
                        break;
                    case 6:
                        builder.append("SRLV");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rt);
                        builder.append(" ");
                        builder.append(rs);
                        break;
                    case 7:
                        builder.append("SRAV");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rt);
                        builder.append(" ");
                        builder.append(rs);
                        break;
                    case 33:
                        builder.append("ADDU");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rs);
                        builder.append(" ");
                        builder.append(rt);
                        break;
                    case 35:
                        builder.append("SUBU");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rs);
                        builder.append(" ");
                        builder.append(rt);
                        break;
                    case 36:
                        builder.append("AND");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rs);
                        builder.append(" ");
                        builder.append(rt);
                        break;
                    case 37:
                        builder.append("OR");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rs);
                        builder.append(" ");
                        builder.append(rt);
                        break;
                    case 38:
                        builder.append("XOR");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rs);
                        builder.append(" ");
                        builder.append(rt);
                        break;
                    case 39:
                        builder.append("NOR");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rs);
                        builder.append(" ");
                        builder.append(rt);
                        break;
                    case 42:
                        builder.append("SLT");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rs);
                        builder.append(" ");
                        builder.append(rt);
                        break;
                    case 8:
                        builder.append("JR");
                        builder.append(" ");
                        builder.append(rs);
                        break;
                    case 9:
                        builder.append("JALR");
                        builder.append(" ");
                        builder.append(rd);
                        builder.append(" ");
                        builder.append(rs);
                        break;
                    default:
                        return null;
                }
                break;
            case 4:
                builder.append("BEQ");
                builder.append(" ");
                builder.append(rs);
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                break;
            case 5:
                builder.append("BNE");
                builder.append(" ");
                builder.append(rs);
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                break;
            case 8:            
                builder.append("ADDI");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(rs);
                builder.append(" ");
                builder.append(offset);
                break;
            case 10:
                builder.append("SLTI");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(rs);
                builder.append(" ");
                builder.append(offset);
                break;
            case 12:
                builder.append("ANDI");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(rs);
                builder.append(" ");
                builder.append(offset);
                break;
            case 13:
                builder.append("ORI");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(rs);
                builder.append(" ");
                builder.append(offset);
                break;
            case 14:
                builder.append("XORI");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(rs);
                builder.append(" ");
                builder.append(offset);
                break;
            case 32:
                builder.append("LB");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                builder.append("(");
                builder.append(rs);
                builder.append(")");
                break;
            case 33:
                builder.append("LH");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                builder.append("(");
                builder.append(rs);
                builder.append(")");
                break;
            case 35:
                builder.append("LW");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                builder.append("(");
                builder.append(rs);
                builder.append(")");
                break;
            case 36:
                builder.append("LBU");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                builder.append("(");
                builder.append(rs);
                builder.append(")");
                break;
            case 37:
                builder.append("LHU");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                builder.append("(");
                builder.append(rs);
                builder.append(")");
                break;
            case 39:
                builder.append("LWU");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                builder.append("(");
                builder.append(rs);
                builder.append(")");
                break;
            case 40:
                builder.append("SB");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                builder.append("(");
                builder.append(rs);
                builder.append(")");
                break;
            case 41:
                builder.append("SH");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                builder.append("(");
                builder.append(rs);
                builder.append(")");
                break;
            case 43:
                builder.append("SW");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                builder.append("(");
                builder.append(rs);
                builder.append(")");
                break;
            case 15:
                builder.append("LUI");
                builder.append(" ");
                builder.append(rt);
                builder.append(" ");
                builder.append(offset);
                break;
            case 2:
                builder.append("J");
                builder.append(" ");
                builder.append(address);
                break;
            case 3:
                builder.append("JAL");
                builder.append(" ");
                builder.append(address);
                break;
            case 63:
                builder.append("H");
                break;
            default:
                return null;
        }
        return builder.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Instructions other = (Instructions) obj;
        if (!Objects.equals(this.opcode, other.opcode)) {
            return false;
        }
        if (!Objects.equals(this.rd, other.rd)) {
            return false;
        }
        if (!Objects.equals(this.rt, other.rt)) {
            return false;
        }
        if (!Objects.equals(this.rs, other.rs)) {
            return false;
        }
        if (!Objects.equals(this.sa, other.sa)) {
            return false;
        }
        if (!Objects.equals(this.offset, other.offset)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.funct, other.funct)) {
            return false;
        }
        return true;
    }

    public byte[] toBytes(){
        byte[] b = new byte[4];
        String binary = getBinary();
        if(binary == null || binary.length() != 32)
            return null;
        for (int i = 0; i < 4; i++) {
            b[i] = (byte) ( Integer.parseInt(binary.substring(i*8,8+i*8),2));
        }
        return b;
    } 
    @Override
    public String toString() {
        return getInstruction();
    }
    
    
}
