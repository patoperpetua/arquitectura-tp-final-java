/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.app.arquitectura.tp.utils;

import ar.com.singleton.app.arquitectura.tp.models.Instructions;
import ar.com.singleton.app.arquitectura.tp.models.Memories;
import ar.com.singleton.app.arquitectura.tp.models.ProcessorStates;
import ar.com.singleton.app.arquitectura.tp.models.ProcessorsData;
import ar.com.singleton.app.arquitectura.tp.models.Signals;
import com.fazecast.jSerialComm.SerialPort;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pato
 */
public class InstructionUtils {
    
    public static final int NUM_REGISTERS = 32;
    public static final int NUM_MEMORIES = 64;
    public static final int NUM_LATCHES = 13;
    public static final int NUM_TOTAL_READ = 4 * (NUM_LATCHES + NUM_MEMORIES + NUM_REGISTERS);//436
    public static List<Instructions> translate(String data){
        if(data == null || data.isEmpty())
            return null;
        String lines[] = data.split("\\r?\\n");
        if(lines == null || lines.length == 0)
            return null;
        List<Instructions> list = new ArrayList<>();
        String attr[];
        String comments;
        Instructions instruction = null;
        int i = 0;
        for(String line : lines){
            if(line.startsWith("#"))
                continue;
            if(line.contains("#")){
                comments = line.split("#")[1];
                line = line.split("#")[0];
            }else
                comments = null;
            line = line.trim().replaceAll(" +", " ");
            attr = line.split(" ");
            if(attr == null || attr.length == 0)
                return null;
            switch(attr[0]){
                case "SLL":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRt(Integer.parseInt(attr[2]));
                    instruction.setSa(Integer.parseInt(attr[3]));
                    instruction.setFunct(0);
                break;
                case "SRL":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRt(Integer.parseInt(attr[2]));
                    instruction.setSa(Integer.parseInt(attr[3]));
                    instruction.setFunct(2);
                break;
                case "SRA":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRt(Integer.parseInt(attr[2]));
                    instruction.setSa(Integer.parseInt(attr[3]));
                    instruction.setFunct(3);
                break;
                case "SLLV":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRt(Integer.parseInt(attr[2]));
                    instruction.setRs(Integer.parseInt(attr[3]));
                    instruction.setFunct(4);
                break;
                case "SRLV":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRt(Integer.parseInt(attr[2]));
                    instruction.setRs(Integer.parseInt(attr[3]));
                    instruction.setFunct(6);
                break;
                case "SRAV":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRt(Integer.parseInt(attr[2]));
                    instruction.setRs(Integer.parseInt(attr[3]));
                    instruction.setFunct(7);
                break;
                case "ADDU":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setRt(Integer.parseInt(attr[3]));
                    instruction.setFunct(33);
                break;
                case "SUBU":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setRt(Integer.parseInt(attr[3]));
                    instruction.setFunct(35);
                break;
                case "AND":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setRt(Integer.parseInt(attr[3]));
                    instruction.setFunct(36);
                break;
                case "OR":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setRt(Integer.parseInt(attr[3]));
                    instruction.setFunct(37);
                break;
                case "XOR":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setRt(Integer.parseInt(attr[3]));
                    instruction.setFunct(38);
                break;
                case "NOR":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setRt(Integer.parseInt(attr[3]));
                    instruction.setFunct(39);
                break;
                case "SLT":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setRt(Integer.parseInt(attr[3]));
                    instruction.setFunct(42);
                break;
                case "JR":
                    if(attr.length != 2){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRs(Integer.parseInt(attr[1]));
                    instruction.setFunct(8);
                break;
                case "JALR":
                    if(attr.length != 3){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(0);
                    instruction.setRd(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setFunct(9);
                break;
                case "LB":
                    if(attr.length != 3 || !attr[2].contains("(") || !attr[2].contains(")")){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(32);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setOffset(Integer.parseInt(attr[2].split("\\(")[0]));
                    instruction.setRs(Integer.parseInt(attr[2].split("\\(")[1].replace(")", "")));
                break;
                case "LH":
                    if(attr.length != 3 || !attr[2].contains("(") || !attr[2].contains(")")){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(33);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setOffset(Integer.parseInt(attr[2].split("\\(")[0]));
                    instruction.setRs(Integer.parseInt(attr[2].split("\\(")[1].replace(")", "")));
                break;
                case "LW":
                    if(attr.length != 3 || !attr[2].contains("(") || !attr[2].contains(")")){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(35);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setOffset(Integer.parseInt(attr[2].split("\\(")[0]));
                    instruction.setRs(Integer.parseInt(attr[2].split("\\(")[1].replace(")", "")));
                break;
                case "LBU":
                    if(attr.length != 3 || !attr[2].contains("(") || !attr[2].contains(")")){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(36);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setOffset(Integer.parseInt(attr[2].split("\\(")[0]));
                    instruction.setRs(Integer.parseInt(attr[2].split("\\(")[1].replace(")", "")));
                break;
                case "LHU":
                    if(attr.length != 3 || !attr[2].contains("(") || !attr[2].contains(")")){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(37);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setOffset(Integer.parseInt(attr[2].split("\\(")[0]));
                    instruction.setRs(Integer.parseInt(attr[2].split("\\(")[1].replace(")", "")));
                break;
                case "LWU":
                    if(attr.length != 3 || !attr[2].contains("(") || !attr[2].contains(")")){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(39);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setOffset(Integer.parseInt(attr[2].split("\\(")[0]));
                    instruction.setRs(Integer.parseInt(attr[2].split("\\(")[1].replace(")", "")));
                break;
                case "SB":
                    if(attr.length != 3 || !attr[2].contains("(") || !attr[2].contains(")")){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(40);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setOffset(Integer.parseInt(attr[2].split("\\(")[0]));
                    instruction.setRs(Integer.parseInt(attr[2].split("\\(")[1].replace(")", "")));
                break;
                case "SH":
                    if(attr.length != 3 || !attr[2].contains("(") || !attr[2].contains(")")){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(41);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setOffset(Integer.parseInt(attr[2].split("\\(")[0]));
                    instruction.setRs(Integer.parseInt(attr[2].split("\\(")[1].replace(")", "")));
                break;
                case "SW":
                    if(attr.length != 3 || !attr[2].contains("(") || !attr[2].contains(")")){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(43);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setOffset(Integer.parseInt(attr[2].split("\\(")[0]));
                    instruction.setRs(Integer.parseInt(attr[2].split("\\(")[1].replace(")", "")));
                break;
                case "ADDI":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(8);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setOffset(Integer.parseInt(attr[3]));
                break;
                case "ANDI":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(12);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setOffset(Integer.parseInt(attr[3]));
                break;
                case "ORI":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(13);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setOffset(Integer.parseInt(attr[3]));
                break;
                case "XORI":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(14);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setOffset(Integer.parseInt(attr[3]));
                break;
                case "LUI":
                    if(attr.length != 3){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(15);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setOffset(Integer.parseInt(attr[2]));
                break;
                case "SLTI":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(10);
                    instruction.setRt(Integer.parseInt(attr[1]));
                    instruction.setRs(Integer.parseInt(attr[2]));
                    instruction.setOffset(Integer.parseInt(attr[3]));
                break;
                case "BEQ":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(4);
                    instruction.setRs(Integer.parseInt(attr[1]));
                    instruction.setRt(Integer.parseInt(attr[2]));
                    instruction.setOffset(Integer.parseInt(attr[3]));
                break;
                case "BNE":
                    if(attr.length != 4){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(5);
                    instruction.setRs(Integer.parseInt(attr[1]));
                    instruction.setRt(Integer.parseInt(attr[2]));
                    instruction.setOffset(Integer.parseInt(attr[3]));
                break;
                case "J":
                    if(attr.length != 2){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(2);
                    instruction.setAddress(Integer.parseInt(attr[1]));
                break;
                case "JAL":
                    if(attr.length != 2){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(3);
                    instruction.setAddress(Integer.parseInt(attr[1]));
                break;
                case "H":
                    if(attr.length != 1){
                        System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                        return null;
                    }
                    instruction = new Instructions();
                    instruction.setOpcode(63);
                    instruction.setAddress(67108863);
                break;
                default:
                    System.out.println("Instruction Nº" + i + ": invalid format. " + line);
                    return null;
            }
            i++;
            instruction.setLine(line);
            instruction.setComments(comments);
            list.add(instruction);
        }
        return list;
    }
    
    public static String translate(List<Instructions> list){
        if(list == null || list.isEmpty())
            return "";
        StringBuilder builder = new StringBuilder();
        for(Instructions inst: list){
            builder.append(inst.getBinary());
            if(inst.getComments() != null)
                builder.append(inst.getComments());
            builder.append("\n");
        }
        return builder.toString();
    }
    
    public static Instructions fromString(String line){
        String comments = null;
        Instructions instruction = new Instructions();
        if(line.startsWith("#"))
            return null;
        if(line.contains("#")){
            comments = line.split("#")[1];
            line = line.split("#")[0];
        }
        line = line.trim();
        if(line.length() != 32)
            return null;
        int opcode = Integer.parseInt(line.substring(0,6), 2);
        instruction.setOpcode(opcode);
        switch(opcode){
            case 0:
                int funct = Integer.parseInt(line.substring(26,32), 2);
                instruction.setFunct(funct);
                switch(funct){
                    case 0:
                    case 2:
                    case 3:
                        instruction.setRt(Integer.parseInt(line.substring(11,16), 2));
                        instruction.setRd(Integer.parseInt(line.substring(16,21), 2));
                        instruction.setSa(Integer.parseInt(line.substring(21,26), 2));
                        break;
                    case 4:
                    case 6:
                    case 7:
                    case 33:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 42:
                        instruction.setRs(Integer.parseInt(line.substring(6,11), 2));
                        instruction.setRt(Integer.parseInt(line.substring(11,16), 2));
                        instruction.setRd(Integer.parseInt(line.substring(16,21), 2));
                        break;
                    case 8:
                        instruction.setRs(Integer.parseInt(line.substring(6,11), 2));
                        break;
                    case 9:
                        instruction.setRs(Integer.parseInt(line.substring(7,11), 2));
                        instruction.setRd(Integer.parseInt(line.substring(17,21), 2));
                        break;
                    default:
                        return null;
                }
                break;
            case 4:
            case 5:
            case 10:    
            case 8:
            case 12:
            case 13:
            case 14:
            case 32:
            case 33:
            case 35:
            case 36:
            case 37:
            case 39:
            case 40:
            case 41:
            case 43:
                instruction.setRs(Integer.parseInt(line.substring(6,11), 2));
                instruction.setRt(Integer.parseInt(line.substring(11,16), 2));
                instruction.setOffset(Integer.parseInt(line.substring(16,32), 2));
                break;
            case 15:
                instruction.setRt(Integer.parseInt(line.substring(11,16), 2));
                instruction.setOffset(Integer.parseInt(line.substring(16,32), 2));
                break;
            case 2:
            case 3:
                instruction.setAddress(Integer.parseInt(line.substring(6,32), 2));
                break;
            case 63:
                instruction.setAddress(67108863);
                break;
            default:
                return null;
        }
        return instruction;
    }
    
    public static boolean isBinaryValid(String data){
        String lines[] = data.split("\\r?\\n");
        if(lines == null || lines.length == 0)
            return false;
        for(String line : lines)
            if(fromString(line) == null)
                return false;
        return true;
    }
    
    public static byte[] getBytes(List<Instructions> inst){
        if(inst == null || inst.isEmpty())
            return null;
        byte[] to_send = new byte[4*inst.size()];
        byte[] recv;
        int i = 0;
        for(Instructions in : inst){
            recv = in.toBytes();
            if(recv == null)
                return null;
            for (int j = 0; j < 4; j++) {
                to_send[i*4+j] = recv[j];
            }
            i++;
        }
        return to_send;
    }
    
    public static boolean sendProgram(List<Instructions> inst, SerialPort port) throws InterruptedException{
        port.closePort();
        if(!port.isOpen())
            if (!port.openPort()){
                System.out.println("Cannot open port. Close programs that are using COM port: " + port.getSystemPortName());
                return false;
            }
        port.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 1000, 1000);
        byte[] to_send = getBytes(inst);
        if(to_send == null || to_send.length == 0)
            return false;
        int sended = 0;
        for (int i = 0; i < to_send.length; i++){
//            while(sended != 1){
    //        for (int i = 0; i < to_send.length; i++) {
    //            while(sended != 1)
    //                sended = port.writeBytes(to_send, 1);
    //        }
                byte[] to = new byte[]{to_send[i]};
                sended = port.writeBytes(to, 1);
//                System.out.println(sended + " - " + i + " - " + to[0]);
                Thread.sleep(200);
//            }
            sended = 0;
        }
        return true;
    }
    
    public static ProcessorStates getState(SerialPort port){
        if(!port.isOpen())
            if (!port.openPort()){
                System.out.println("Cannot open port. Close programs that are using COM port: " + port.getSystemPortName());
                return ProcessorStates.ERROR;
            }
        port.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 1000, 0);
        byte[] readBuffer = new byte[1];
        int numRead = 0;
        try {
            while (numRead == 0)
            {
                port.writeBytes(new byte[]{(byte)Signals.SIG_START}, 1);
                numRead = port.readBytes(readBuffer, 1);
            }
        } catch (Exception e) { e.printStackTrace(); }
        if(readBuffer[0] == Signals.SIG_START)
            return ProcessorStates.PROGRAMMING;
        else
            return ProcessorStates.ERROR;
    }
    
    public static ProcessorStates sendReProgram(SerialPort port){
        if(!port.isOpen())
            if (!port.openPort()){
                System.out.println("Cannot open port. Close programs that are using COM port: " + port.getSystemPortName());
                return ProcessorStates.ERROR;
            }
        port.setComPortTimeouts(SerialPort.TIMEOUT_WRITE_BLOCKING, 1000, 0);
        port.writeBytes(new byte[]{(byte)Signals.SIG_RE_PROGRAM}, 1);
//        byte[] readBuffer = new byte[1];
//        int numRead = 0;
//        try {
//            while (numRead == 0)
//            {
//                port.writeBytes(new byte[]{(byte)Signals.SIG_RE_PROGRAM}, 1);
//                numRead = port.readBytes(readBuffer, 1);
//            }
//        } catch (Exception e) { e.printStackTrace(); }
//        if(readBuffer[0] == Signals.SIG_START)
            return ProcessorStates.PROGRAMMING;
//        else
//            return ProcessorStates.ERROR;
    }
    
    public static ProcessorStates sendStepSignal(SerialPort port){
        if(!port.isOpen())
            if (!port.openPort()){
                System.out.println("Cannot open port. Close programs that are using COM port: " + port.getSystemPortName());
                return ProcessorStates.ERROR;
            }
        port.setComPortTimeouts(SerialPort.TIMEOUT_WRITE_BLOCKING, 1000, 0);
        port.writeBytes(new byte[]{(byte)Signals.SIG_STEP_BY_STEP}, 1);
//        byte[] readBuffer = new byte[1];
//        int numRead = 0;
//        try {
//            while (numRead == 0)
//            {
//                port.writeBytes(new byte[]{(byte)Signals.SIG_RE_PROGRAM}, 1);
//                numRead = port.readBytes(readBuffer, 1);
//            }
//        } catch (Exception e) { e.printStackTrace(); }
//        if(readBuffer[0] == Signals.SIG_START)
            return ProcessorStates.STEP_BY_SETEP;
//        else
//            return ProcessorStates.ERROR;
    }
    
    public static ProcessorStates sendContinuousSignal(SerialPort port){
        if(!port.isOpen())
            if (!port.openPort()){
                System.out.println("Cannot open port. Close programs that are using COM port: " + port.getSystemPortName());
                return ProcessorStates.ERROR;
            }
        port.setComPortTimeouts(SerialPort.TIMEOUT_WRITE_BLOCKING, 1000, 0);
        port.writeBytes(new byte[]{(byte)Signals.SIG_CONTINUOUS}, 1);
        
//        byte[] readBuffer = new byte[1];
//        int numRead = 0;
//        try {
//            while (numRead == 0)
//            {
//                port.writeBytes(new byte[]{(byte)Signals.SIG_RE_PROGRAM}, 1);
//                numRead = port.readBytes(readBuffer, 1);
//            }
//        } catch (Exception e) { e.printStackTrace(); }
//        if(readBuffer[0] == Signals.SIG_START)
            return ProcessorStates.WAITING;
//        else
//            return ProcessorStates.ERROR;
    }

    public static ProcessorStates sendNextStepSignal(SerialPort port){
        if(!port.isOpen())
            if (!port.openPort()){
                System.out.println("Cannot open port. Close programs that are using COM port: " + port.getSystemPortName());
                return ProcessorStates.ERROR;
            }
        port.setComPortTimeouts(SerialPort.TIMEOUT_WRITE_BLOCKING, 1000, 0);
        port.writeBytes(new byte[]{(byte)Signals.SIG_NEXT_STEP}, 1);
//        byte[] readBuffer = new byte[1];
//        int numRead = 0;
//        try {
//            while (numRead == 0)
//            {
//                port.writeBytes(new byte[]{(byte)Signals.SIG_RE_PROGRAM}, 1);
//                numRead = port.readBytes(readBuffer, 1);
//            }
//        } catch (Exception e) { e.printStackTrace(); }
//        if(readBuffer[0] == Signals.SIG_START)
            return ProcessorStates.STEP_BY_SETEP;
//        else
//            return ProcessorStates.ERROR;
    }
    
    public static byte[] readData(SerialPort port) {
        if(!port.isOpen())
            if (!port.openPort()){
                System.out.println("Cannot open port. Close programs that are using COM port: " + port.getSystemPortName());
                return null;
            }
        port.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 1000, 0);
        byte[] readBuffer = new byte[NUM_TOTAL_READ];
        int numRead = 0;
        while (numRead != readBuffer.length)
        {
            numRead = port.readBytes(readBuffer, readBuffer.length);
//            System.out.println(numRead);
        }
        return readBuffer;
    }
    
    public static ProcessorsData readMemories(SerialPort port){
        byte[] data = readData(port);
        return readMemories(data);
    }
    
    public static ProcessorsData readMemories(byte[] data){
        ProcessorsData processorsData = new ProcessorsData();
        for (int i = 0; i < data.length; i = i+4) {
            switch(i/4){
                case 0:
                    processorsData.addLatchIF(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "Inst"));
                break;
                case 1:
                    processorsData.addLatchIF(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "Addr PC"));
                break;
                case 2:
                    processorsData.addLatchID(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "RT DATA"));
                break;
                case 3:
                    processorsData.addLatchID(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "RS DATA"));
                break;
                case 4:
                    processorsData.addLatchID(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "SIGN"));
                break;
                case 5:
                    System.out.println("ID: " + data[i] + ", " + data[i+1] + ", " + data[i+2] + ", " + data[i+3]);
                    processorsData.addLatchID(new Memories(((data[i+3]&   0b10000000) >> 7), "REG WR"));
                    processorsData.addLatchID(new Memories(((data[i+3]&   0b00011111)) , "REG DST"));
                    processorsData.addLatchID(new Memories(((data[i+3]&   0b01000000) >> 6) , "MEM WR"));
                    processorsData.addLatchID(new Memories(((data[i+3]&   0b00100000) >> 5) , "MEM RD"));
                    processorsData.addLatchID(new Memories(((data[i+2]&   0b11110000) >> 4) , "ALU SEL"));
                    processorsData.addLatchID(new Memories((((data[i+2]&  0b00001111) << 1) | ((data[i+1] & 0b10000000) >> 7) ), "SHAMPT"));//8
                    processorsData.addLatchID(new Memories(((data[i+1]&   0b01111100) >> 2) , "RT ADDR"));
                    processorsData.addLatchID(new Memories(((data[i]&     0b11111000) >> 3) , "RS ADDR"));
                    processorsData.addLatchID(new Memories(((data[i+1]&   0b00000001)) , "HALT"));
                    processorsData.addLatchID(new Memories(((data[i+1]&   0b00000010)) , "INST SEL"));
                break;
                case 6:
                    processorsData.addLatchID(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "Addr Branch"));
                break;
                case 7:
                    processorsData.addLatchID(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "Inst ID"));
                break;
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                    processorsData.addRegister(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "R"+ (i/4-8)));
                break;
                case 40:
                    processorsData.addLatchEX(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "ALU OUT"));
                    break;
                case 41:
                    System.out.println("EX: " + data[i]);
                    processorsData.addLatchEX(new Memories(byteAsULong((byte) (data[i+1])) , "HALT"));
                    processorsData.addLatchEX(new Memories(byteAsULong((byte) ((data[i] &   0b10000000) >> 7)) , "MEM RD"));
                    processorsData.addLatchEX(new Memories(byteAsULong((byte) ((data[i] &   0b01000000) >> 6)) , "MEM WR"));
                    processorsData.addLatchEX(new Memories(byteAsULong((byte) ((data[i] &   0b00111110) >> 1)) , "REG DST"));
                    processorsData.addLatchEX(new Memories(byteAsULong((byte) (data[i] &   0b00000001)) , "REG WR"));
                    break;
                case 42:
                    processorsData.addLatchMEM(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "REG Data"));
                    break;
                case 43:
                    System.out.println("MEM: " + data[i]);
                    processorsData.setHalt(new Memories(byteAsULong((byte) ((data[i] &   0b10000000) >> 7)) , "HALT"));
                    processorsData.addLatchMEM(new Memories(byteAsULong((byte) ((data[i] &   0b01000000) >> 6)) , "HALT"));
                    processorsData.addLatchMEM(new Memories(byteAsULong((byte) ((data[i] &   0b00100000) >> 5)) , "REG WR"));
                    processorsData.addLatchMEM(new Memories(byteAsULong((byte) (data[i] &   0b00011111)) , "REG DST"));
                    break;
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                case 68:
                case 69:
                case 70:
                case 71:
                case 72:
                case 73:
                case 74:
                case 75:
                case 76:
                case 77:
                case 78:
                case 79:
                case 80:
                case 81:
                case 82:
                case 83:
                case 84:
                case 85:
                case 86:
                case 87:
                case 88:
                case 89:
                case 90:
                case 91:
                case 92:
                case 93:
                case 94:
                case 95:
                case 96:
                case 97:
                case 98:
                case 99:
                case 100:
                case 101:
                case 102:
                case 103:
                case 104:
                case 105:
                case 106:
                case 107:
                    processorsData.addMemory(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "M"+ (i/4-44)));
                    break;
                case 108:
                    processorsData.setClock(new Memories(byteAsULong(data[i]) | (byteAsULong(data[i+1]) << 8) | (byteAsULong(data[i+2]) << 16) | (byteAsULong(data[i+3]) << 24), "Clocks"));
                    break;
                default:
                    return null;
            }
        }
        return processorsData;
    }
    
    public static long byteAsULong(byte b) {
        return ((long)b) & 0x00000000000000FFL; 
    }
}
