/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.app.arquitectura.tp.models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pato
 */
public class ProcessorsData {
    private List<Memories> registers;
    private List<Memories> memory;
    private List<Memories> latchIF;
    private List<Memories> latchID;
    private List<Memories> latchEX;
    private List<Memories> latchMEM;
    private Memories clock;
    private Memories halt;
    
    public ProcessorsData() {
        registers = new ArrayList<>();
        memory = new ArrayList<>();
        latchIF = new ArrayList<>();
        latchID = new ArrayList<>();
        latchEX = new ArrayList<>();
        latchMEM = new ArrayList<>();
        clock = new Memories(0, "Clock");
        halt = new Memories(0, "WR HALT");
    }

    public Memories getClock() {
        return clock;
    }

    public void setClock(Memories clock) {
        this.clock = clock;
    }
    
    public void addRegister(Memories mem){
        registers.add(mem);
    }
    
    public void addMemory(Memories mem){
        memory.add(mem);
    }
    
    public void addLatchIF(Memories mem){
        latchIF.add(mem);
    }
    
    public void addLatchID(Memories mem){
        latchID.add(mem);
    }
    
    public void addLatchEX(Memories mem){
        latchEX.add(mem);
    }
    
    public void addLatchMEM(Memories mem){
        latchMEM.add(mem);
    }

    public List<Memories> getRegisters() {
        return registers;
    }

    public void setRegisters(List<Memories> registers) {
        this.registers = registers;
    }

    public List<Memories> getMemory() {
        return memory;
    }

    public void setMemory(List<Memories> memory) {
        this.memory = memory;
    }

    public List<Memories> getLatchIF() {
        return latchIF;
    }

    public void setLatchIF(List<Memories> latchIF) {
        this.latchIF = latchIF;
    }

    public List<Memories> getLatchID() {
        return latchID;
    }

    public void setLatchID(List<Memories> latchID) {
        this.latchID = latchID;
    }

    public List<Memories> getLatchEX() {
        return latchEX;
    }

    public void setLatchEX(List<Memories> latchEX) {
        this.latchEX = latchEX;
    }

    public List<Memories> getLatchMEM() {
        return latchMEM;
    }

    public void setLatchMEM(List<Memories> latchMEM) {
        this.latchMEM = latchMEM;
    }

    public Memories getHalt() {
        return halt;
    }

    public void setHalt(Memories halt) {
        this.halt = halt;
    }
    
}
