/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.app.arquitectura.tp.models;

import com.fazecast.jSerialComm.SerialPort;

/**
 *
 * @author pato
 */
public class SerialPorts {
    private SerialPort port;
    private String name;
    
    public SerialPorts() {
    }

    public SerialPorts(SerialPort port) {
        this.port = port;
    }

    public SerialPorts(String name) {
        this.name = name;
    }
    
    public SerialPort getPort() {
        return port;
    }

    public void setPort(SerialPort port) {
        this.port = port;
    }

    @Override
    public String toString() {
        if(name != null)
            return name;
        if(port == null)
            return "";
        return port.getSystemPortName();
    }
    
}
