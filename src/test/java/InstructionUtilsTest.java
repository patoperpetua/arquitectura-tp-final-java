/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ar.com.singleton.app.arquitectura.tp.models.Instructions;
import ar.com.singleton.app.arquitectura.tp.models.ProcessorsData;
import ar.com.singleton.app.arquitectura.tp.utils.InstructionUtils;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pato
 */
public class InstructionUtilsTest {
    
    final String inst;
    final String decode;
    final String decodes[];
    final byte[] bytes;
    final byte[] read;
    List<Instructions> list;
    
    public InstructionUtilsTest() {
        inst = "#Comentario inofencivo \n"
            + "SLL  5  9  2 \n"
            + "SRL 5  9 2 \n"
            + "SRA 5 9 2 #comentario\n"
            
            + "SLLV 5 9 2 \n"
            + "SRLV 5 9 2 \n"
            + "SRAV 5 9 2 \n"
            
            + "ADDU 5 2 9 \n"
            + "SUBU 5 2 9 \n"
            + "AND 5 2 9 \n"
            + "OR 5 2 9 \n"
            + "XOR 5 2 9 \n"
            + "NOR 5 2 9 \n"
            + "SLT 5 2 9 \n"
                
            + "JR 7 \n"
            + "JALR 3 8 \n"
            
            + "LB 6 2(3) \n"
            + "LH 6 2(3) \n"
            + "LW 6 2(3) \n"
            + "LBU 6 2(3) \n"
            + "LHU 6 2(3) \n"
            + "LWU 6 2(3) \n"
            
            + "SB 6 2(3) \n"
            + "SH 6 2(3) \n"
            + "SW 6 2(3) \n"
            
            + "ADDI 5 9 25 \n"
            + "ANDI 5 9 25 \n"
            + "ORI 5 9 25 \n"
            + "XORI 5 9 25 \n"
            + "LUI 5 25 \n"
            + "SLTI 5 9 25 \n"
            
            + "BEQ 5 11 40 \n"
            + "BNE 5 11 40 \n"
            
            + "J 2500 \n"
            + "JAL 2500 \n"
            + "H \n";
        decode =   "000000 00000 01001 00101 00010 000000 \n"
                +  "000000 00000 01001 00101 00010 000010 \n"
                +  "000000 00000 01001 00101 00010 000011 \n"
                
                +  "000000 00010 01001 00101 00000 000100 \n"
                +  "000000 00010 01001 00101 00000 000110 \n"
                +  "000000 00010 01001 00101 00000 000111 \n"
                
                +  "000000 00010 01001 00101 00000 100001 \n"
                +  "000000 00010 01001 00101 00000 100011 \n"
                +  "000000 00010 01001 00101 00000 100100 \n"
                +  "000000 00010 01001 00101 00000 100101 \n"
                +  "000000 00010 01001 00101 00000 100110 \n"
                +  "000000 00010 01001 00101 00000 100111 \n"
                +  "000000 00010 01001 00101 00000 101010 \n"
                
                +  "000000 00111 00000 00000 00000 001000 \n"
                +  "000000 01000 00000 00011 00000 001001 \n"
                
                +  "100000 00011 00110 0000 0000 0000 0010 \n"
                +  "100001 00011 00110 0000 0000 0000 0010 \n"
                +  "100011 00011 00110 0000 0000 0000 0010 \n"
                +  "100100 00011 00110 0000 0000 0000 0010 \n"
                +  "100101 00011 00110 0000 0000 0000 0010 \n"
                +  "100111 00011 00110 0000 0000 0000 0010 \n"
                
                +  "101000 00011 00110 0000 0000 0000 0010 \n"
                +  "101001 00011 00110 0000 0000 0000 0010 \n"
                +  "101011 00011 00110 0000 0000 0000 0010 \n"
                
                +  "001000 01001 00101 0000 0000 0001 1001 \n"
                +  "001100 01001 00101 0000 0000 0001 1001 \n"
                +  "001101 01001 00101 0000 0000 0001 1001 \n"
                +  "001110 01001 00101 0000 0000 0001 1001 \n"
                +  "001111 00000 00101 0000 0000 0001 1001 \n"
                +  "001010 01001 00101 0000 0000 0001 1001 \n"
                
                +  "000100 00101 01011 0000 0000 0010 1000 \n"
                +  "000101 00101 01011 0000 0000 0010 1000 \n"
                
                +  "000010 0000 0000 0000 0010 0111 0001 00 \n"
                +  "000011 0000 0000 0000 0010 0111 0001 00 \n"
                +  "1111 1111 1111 1111 1111 1111 1111 1111 \n"
                ;
        decodes = decode.split("\\r?\\n");
        bytes = new byte[140];
        bytes[0] = 0;
        bytes[1] = 9;
        bytes[2] = 40;
        bytes[3] = (byte) 128;
        
        bytes[4] = 0;
        bytes[5] = 9;
        bytes[6] = 40;
        bytes[7] = (byte) 130;
        
        bytes[8] = 0;
        bytes[9] = 9;
        bytes[10] = 40;
        bytes[11] = (byte) 131;
        
        bytes[12] = 0;
        bytes[13] = 73;
        bytes[14] = 40;
        bytes[15] = 4;
        
        bytes[16] = 0;
        bytes[17] = 73;
        bytes[18] = 40;
        bytes[19] = 6;
        
        bytes[20] = 0;
        bytes[21] = 73;
        bytes[22] = 40;
        bytes[23] = 7;
        
        bytes[24] = 0;
        bytes[25] = 73;
        bytes[26] = 40;
        bytes[27] = 33;
        
        bytes[28] = 0;
        bytes[29] = 73;
        bytes[30] = 40;
        bytes[31] = 35;
        
        bytes[32] = 0;
        bytes[33] = 73;
        bytes[34] = 40;
        bytes[35] = 36;
        
        bytes[36] = 0;
        bytes[37] = 73;
        bytes[38] = 40;
        bytes[39] = 37;
        
        bytes[40] = 0;
        bytes[41] = 73;
        bytes[42] = 40;
        bytes[43] = 38;
        
        bytes[44] = 0;
        bytes[45] = 73;
        bytes[46] = 40;
        bytes[47] = 39;
        
        bytes[48] = 0;
        bytes[49] = 73;
        bytes[50] = 40;
        bytes[51] = 42;
        
        bytes[52] = 0;
        bytes[53] = (byte) 224;
        bytes[54] = 0;
        bytes[55] = 8;
        
        bytes[56] = 1;
        bytes[57] = 0;
        bytes[58] = 24;
        bytes[59] = 9;
        
        bytes[60] = (byte) 128;
        bytes[61] = 102;
        bytes[62] = 0;
        bytes[63] = 2;
        
        bytes[64] = (byte) 132;
        bytes[65] = 102;
        bytes[66] = 0;
        bytes[67] = 2;
        
        bytes[68] = (byte) 140;
        bytes[69] = 102;
        bytes[70] = 0;
        bytes[71] = 2;
        
        bytes[72] = (byte) 144;
        bytes[73] = 102;
        bytes[74] = 0;
        bytes[75] = 2;
        
        bytes[76] = (byte) 148;
        bytes[77] = 102;
        bytes[78] = 0;
        bytes[79] = 2;
        
        bytes[80] = (byte) 156;
        bytes[81] = 102;
        bytes[82] = 0;
        bytes[83] = 2;
        
        bytes[84] = (byte) 160;
        bytes[85] = 102;
        bytes[86] = 0;
        bytes[87] = 2;
        
        bytes[88] = (byte) 164;
        bytes[89] = 102;
        bytes[90] = 0;
        bytes[91] = 2;
        
        bytes[92] = (byte) 172;
        bytes[93] = 102;
        bytes[94] = 0;
        bytes[95] = 2;
        
        bytes[96] = 33;
        bytes[97] = 37;
        bytes[98] = 0;
        bytes[99] = 25;
        
        bytes[100] = 49;
        bytes[101] = 37;
        bytes[102] = 0;
        bytes[103] = 25;
        
        bytes[104] = 53;
        bytes[105] = 37;
        bytes[106] = 0;
        bytes[107] = 25;
        
        bytes[108] = 57;
        bytes[109] = 37;
        bytes[110] = 0;
        bytes[111] = 25;
        
        bytes[112] = 60;
        bytes[113] = 5;
        bytes[114] = 0;
        bytes[115] = 25;
        
        bytes[116] = 41;
        bytes[117] = 37;
        bytes[118] = 0;
        bytes[119] = 25;
        
        bytes[120] = 16;
        bytes[121] = (byte) 171;
        bytes[122] = 0;
        bytes[123] = 40;
        
        bytes[124] = 20;
        bytes[125] = (byte) 171;
        bytes[126] = 0;
        bytes[127] = 40;
        
        bytes[128] = 8;
        bytes[129] = 0;
        bytes[130] = 9;
        bytes[131] = (byte) 196;
        
        bytes[132] = 12;
        bytes[133] = 0;
        bytes[134] = 9;
        bytes[135] = (byte) 196;
        
        bytes[136] = (byte) 255;
        bytes[137] = (byte) 255;
        bytes[138] = (byte) 255;
        bytes[139] = (byte) 255;
        
        read = new byte[InstructionUtils.NUM_TOTAL_READ];
        
        //Instruction = 
        read[00] = (byte) 0;
        read[01] = (byte) 1;
        read[02] = (byte) 2;
        read[03] = (byte) 3;
        //Address PC = 
        read[04] = (byte) 1;
        read[05] = (byte) 2;
        read[06] = (byte) 3;
        read[07] = (byte) 4;
        //Reg RT = 
        read[8] = (byte)  2;
        read[9] = (byte)  3;
        read[10] = (byte) 4;
        read[11] = (byte) 5;
        //Reg RS = 
        read[12] = (byte) 3;
        read[13] = (byte) 4;
        read[14] = (byte) 5;
        read[15] = (byte) 6;
        //Sign = 
        read[16] = (byte) 4;
        read[17] = (byte) 5;
        read[18] = (byte) 6;
        read[19] = (byte) 7;
        //Special Flag 1: 
        //REG_RW_ID_RX = 1, MEM_RW_ID_EX = 0, MEM_RD_ID_EX = 1, REG_DST_ID_EX = 10
        //ALU_SELECTOR = 7, SHAMPT = 25
        //RT = 4
        //RS = 9
        read[20] = (byte) 0xAA;
        read[21] = (byte) 0x7C;
        read[22] = (byte) 0x90;
        read[23] = (byte) 0x48;
        //Address Branch = 
        read[24] = (byte) 5;
        read[25] = (byte) 6;
        read[26] = (byte) 7;
        read[27] = (byte) 8;
        // Inst ID = 
        read[28] = (byte) 6;
        read[29] = (byte) 7;
        read[30] = (byte) 8;
        read[31] = (byte) 9;
        // R0 = 
        read[32] = (byte) 7;
        read[33] = (byte) 8;
        read[34] = (byte) 9;
        read[35] = (byte) 10;
        // R1 =
        read[36] = (byte) 8;
        read[37] = (byte) 9;
        read[38] = (byte) 10;
        read[39] = (byte) 11;
        // R2 = 
        read[40] = (byte) 9;
        read[41] = (byte) 10;
        read[42] = (byte) 11;
        read[43] = (byte) 12;
        // R3 = 
        read[44] = (byte) 10;
        read[45] = (byte) 11;
        read[46] = (byte) 12;
        read[47] = (byte) 13;
        // R4 = 
        read[48] = (byte) 11;
        read[49] = (byte) 12;
        read[50] = (byte) 13;
        read[51] = (byte) 14;
        // R5 = 
        read[52] = (byte) 12;
        read[53] = (byte) 13;
        read[54] = (byte) 14;
        read[55] = (byte) 15;
        // R6 = 
        read[56] = (byte) 13;
        read[57] = (byte) 14;
        read[58] = (byte) 15;
        read[59] = (byte) 16;
        // R7 = 
        read[60] = (byte) 14;
        read[61] = (byte) 15;
        read[62] = (byte) 16;
        read[63] = (byte) 17;
        // R8 = 
        read[64] = (byte) 15;
        read[65] = (byte) 16;
        read[66] = (byte) 17;
        read[67] = (byte) 18;
        // R9 = 
        read[68] = (byte) 16;
        read[69] = (byte) 17;
        read[70] = (byte) 18;
        read[71] = (byte) 19;
        // R10 = 
        read[72] = (byte) 17;
        read[73] = (byte) 18;
        read[74] = (byte) 19;
        read[75] = (byte) 20;
        // R11 = 
        read[76] = (byte) 18;
        read[77] = (byte) 19;
        read[78] = (byte) 20;
        read[79] = (byte) 21;
        // R12 = 
        read[80] = (byte) 19;
        read[81] = (byte) 20;
        read[82] = (byte) 21;
        read[83] = (byte) 22;
        // R13 = 
        read[84] = (byte) 20;
        read[85] = (byte) 21;
        read[86] = (byte) 22;
        read[87] = (byte) 23;
        // R14 = 
        read[88] = (byte) 21;
        read[89] = (byte) 22;
        read[90] = (byte) 23;
        read[91] = (byte) 24;
        // R15 = 
        read[92] = (byte) 22;
        read[93] = (byte) 23;
        read[94] = (byte) 24;
        read[95] = (byte) 25;
        // R16 = 
        read[96] = (byte) 23;
        read[97] = (byte) 24;
        read[98] = (byte) 25;
        read[99] = (byte) 26;
        // R17 = 
        read[100] = (byte) 24;
        read[101] = (byte) 25;
        read[102] = (byte) 26;
        read[103] = (byte) 27;
        // R18 = 
        read[104] = (byte) 25;
        read[105] = (byte) 26;
        read[106] = (byte) 27;
        read[107] = (byte) 28;
        // R19 = 
        read[108] = (byte) 26;
        read[109] = (byte) 27;
        read[110] = (byte) 28;
        read[111] = (byte) 29;
        // R20 = 
        read[112] = (byte) 27;
        read[113] = (byte) 28;
        read[114] = (byte) 29;
        read[115] = (byte) 30;
        // R21 = 
        read[116] = (byte) 28;
        read[117] = (byte) 29;
        read[118] = (byte) 30;
        read[119] = (byte) 31;
        // R22 = 
        read[120] = (byte) 29;
        read[121] = (byte) 30;
        read[122] = (byte) 31;
        read[123] = (byte) 32;
        // R23 = 
        read[124] = (byte) 30;
        read[125] = (byte) 31;
        read[126] = (byte) 32;
        read[127] = (byte) 33;
        // R24 = 
        read[128] = (byte) 31;
        read[129] = (byte) 32;
        read[130] = (byte) 33;
        read[131] = (byte) 34;
        // R25 = 
        read[132] = (byte) 32;
        read[133] = (byte) 33;
        read[134] = (byte) 34;
        read[135] = (byte) 35;
        // R26 = 
        read[136] = (byte) 33;
        read[137] = (byte) 34;
        read[138] = (byte) 35;
        read[139] = (byte) 36;
        // R27 = 
        read[140] = (byte) 34;
        read[141] = (byte) 35;
        read[142] = (byte) 36;
        read[143] = (byte) 37;
        // R28 = 
        read[144] = (byte) 35;
        read[145] = (byte) 36;
        read[146] = (byte) 37;
        read[147] = (byte) 38;
        // R29 = 
        read[148] = (byte) 36;
        read[149] = (byte) 37;
        read[150] = (byte) 38;
        read[151] = (byte) 39;
        // R30 = 
        read[152] = (byte) 37;
        read[153] = (byte) 38;
        read[154] = (byte) 39;
        read[155] = (byte) 40;
        // R31 = 
        read[156] = (byte) 38;
        read[157] = (byte) 39;
        read[158] = (byte) 40;
        read[159] = (byte) 41;
        // AluResult
        read[160] = (byte) 42;
        read[161] = (byte) 43;
        read[162] = (byte) 44;
        read[163] = (byte) 45;
        // Special Flag 2.
        // MEM_RD = 0, MEM_WR = 1, REG_DST=7, REG_WR=0
        read[164] = (byte) 0x00;
        read[165] = (byte) 0x00;
        read[166] = (byte) 0x00;
        read[167] = (byte) 0x4E;
        // RegToWrite = 
        read[168] = (byte) 43;
        read[169] = (byte) 44;
        read[170] = (byte) 45;
        read[171] = (byte) 46;
        // Special Flag 3.
        //Halt=1, REG_WR=0, REG_DST=27
        read[172] = (byte) 0x00;
        read[173] = (byte) 0x00;
        read[174] = (byte) 0x00;
        read[175] = (byte) 0x5B;
        //M00 = 0
        read[176] = (byte) 44;
        read[177] = (byte) 45;
        read[178] = (byte) 46;
        read[179] = (byte) 47;
        //M01 = 
        read[180] = (byte) 45;
        read[181] = (byte) 46;
        read[182] = (byte) 47;
        read[183] = (byte) 48;
        //M02 = 
        read[184] = (byte) 46;
        read[185] = (byte) 47;
        read[186] = (byte) 48;
        read[187] = (byte) 49;
        //M03 = 
        read[188] = (byte) 47;
        read[189] = (byte) 48;
        read[190] = (byte) 49;
        read[191] = (byte) 50;
        //M04 = 
        read[192] = (byte) 48;
        read[193] = (byte) 49;
        read[194] = (byte) 50;
        read[195] = (byte) 51;
        //M05 = 
        read[196] = (byte) 49;
        read[197] = (byte) 50;
        read[198] = (byte) 51;
        read[199] = (byte) 52;
        //M06 = 
        read[200] = (byte) 50;
        read[201] = (byte) 51;
        read[202] = (byte) 52;
        read[203] = (byte) 53;
        //M07 = 
        read[204] = (byte) 51;
        read[205] = (byte) 52;
        read[206] = (byte) 53;
        read[207] = (byte) 54;
        //M08 = 
        read[208] = (byte) 52;
        read[209] = (byte) 53;
        read[210] = (byte) 54;
        read[211] = (byte) 55;
        //M09 = 
        read[212] = (byte) 53;
        read[213] = (byte) 54;
        read[214] = (byte) 55;
        read[215] = (byte) 56;
        //M10 = 
        read[216] = (byte) 54;
        read[217] = (byte) 55;
        read[218] = (byte) 56;
        read[219] = (byte) 57;
        //M11 = 
        read[220] = (byte) 55;
        read[221] = (byte) 56;
        read[222] = (byte) 57;
        read[223] = (byte) 58;
        //M12 = 
        read[224] = (byte) 56;
        read[225] = (byte) 57;
        read[226] = (byte) 58;
        read[227] = (byte) 59;
        //M13 = 
        read[228] = (byte) 57;
        read[229] = (byte) 58;
        read[230] = (byte) 59;
        read[231] = (byte) 60;
        //M14 = 
        read[232] = (byte) 58;
        read[233] = (byte) 59;
        read[234] = (byte) 60;
        read[235] = (byte) 61;
        //M15 = 
        read[236] = (byte) 59;
        read[237] = (byte) 60;
        read[238] = (byte) 61;
        read[239] = (byte) 62;
        //M16 = 
        read[240] = (byte) 60;
        read[241] = (byte) 61;
        read[242] = (byte) 62;
        read[243] = (byte) 63;
        //M17 = 
        read[244] = (byte) 61;
        read[245] = (byte) 62;
        read[246] = (byte) 63;
        read[247] = (byte) 64;
        //M18 = 
        read[248] = (byte) 62;
        read[249] = (byte) 63;
        read[250] = (byte) 64;
        read[251] = (byte) 65;
        //M19 = 
        read[252] = (byte) 63;
        read[253] = (byte) 64;
        read[254] = (byte) 65;
        read[255] = (byte) 66;
        //M20 = 
        read[256] = (byte) 64;
        read[257] = (byte) 65;
        read[258] = (byte) 66;
        read[259] = (byte) 67;
        //M21 = 
        read[260] = (byte) 65;
        read[261] = (byte) 66;
        read[262] = (byte) 67;
        read[263] = (byte) 68;
        //M22 = 
        read[264] = (byte) 66;
        read[265] = (byte) 67;
        read[266] = (byte) 68;
        read[267] = (byte) 69;
        //M23 = 
        read[268] = (byte) 67;
        read[269] = (byte) 68;
        read[270] = (byte) 69;
        read[271] = (byte) 70;
        //M24 = 
        read[272] = (byte) 68;
        read[273] = (byte) 69;
        read[274] = (byte) 70;
        read[275] = (byte) 71;
        //M25 = 
        read[276] = (byte) 69;
        read[277] = (byte) 70;
        read[278] = (byte) 71;
        read[279] = (byte) 72;
        //M26 = 
        read[280] = (byte) 70;
        read[281] = (byte) 71;
        read[282] = (byte) 72;
        read[283] = (byte) 73;
        //M27 = 
        read[284] = (byte) 71;
        read[285] = (byte) 72;
        read[286] = (byte) 73;
        read[287] = (byte) 74;
        //M28 = 
        read[288] = (byte) 72;
        read[289] = (byte) 73;
        read[290] = (byte) 74;
        read[291] = (byte) 75;
        //M29 = 
        read[292] = (byte) 73;
        read[293] = (byte) 74;
        read[294] = (byte) 75;
        read[295] = (byte) 76;
        //M30 = 
        read[296] = (byte) 74;
        read[297] = (byte) 75;
        read[298] = (byte) 76;
        read[299] = (byte) 77;
        //M31 = 
        read[300] = (byte) 75;
        read[301] = (byte) 76;
        read[302] = (byte) 77;
        read[303] = (byte) 78;
        //M32 = 
        read[304] = (byte) 76;
        read[305] = (byte) 77;
        read[306] = (byte) 78;
        read[307] = (byte) 79;
        //M33 = 
        read[308] = (byte) 77;
        read[309] = (byte) 78;
        read[310] = (byte) 79;
        read[311] = (byte) 80;
        //M34 = 
        read[312] = (byte) 78;
        read[313] = (byte) 79;
        read[314] = (byte) 80;
        read[315] = (byte) 81;
        //M35 = 
        read[316] = (byte) 79;
        read[317] = (byte) 80;
        read[318] = (byte) 81;
        read[319] = (byte) 82;
        //M36 = 
        read[320] = (byte) 80;
        read[321] = (byte) 81;
        read[322] = (byte) 82;
        read[323] = (byte) 83;
        //M37 = 
        read[324] = (byte) 81;
        read[325] = (byte) 82;
        read[326] = (byte) 83;
        read[327] = (byte) 84;
        //M38 = 
        read[328] = (byte) 82;
        read[329] = (byte) 83;
        read[330] = (byte) 84;
        read[331] = (byte) 85;
        //M39 = 
        read[332] = (byte) 83;
        read[333] = (byte) 84;
        read[334] = (byte) 85;
        read[335] = (byte) 86;
        //M40 = 
        read[336] = (byte) 84;
        read[337] = (byte) 85;
        read[338] = (byte) 86;
        read[339] = (byte) 87;
        //M41 = 
        read[340] = (byte) 85;
        read[341] = (byte) 86;
        read[342] = (byte) 87;
        read[343] = (byte) 88;
        //M42 = 
        read[344] = (byte) 86;
        read[345] = (byte) 87;
        read[346] = (byte) 88;
        read[347] = (byte) 89;
        //M43 = 
        read[348] = (byte) 87;
        read[349] = (byte) 88;
        read[350] = (byte) 89;
        read[351] = (byte) 90;
        //M44 = 
        read[352] = (byte) 88;
        read[353] = (byte) 89;
        read[354] = (byte) 90;
        read[355] = (byte) 91;
        //M45 = 
        read[356] = (byte) 89;
        read[357] = (byte) 90;
        read[358] = (byte) 91;
        read[359] = (byte) 92;
        //M46 = 
        read[360] = (byte) 90;
        read[361] = (byte) 91;
        read[362] = (byte) 92;
        read[363] = (byte) 93;
        //M47 = 
        read[364] = (byte) 91;
        read[365] = (byte) 92;
        read[366] = (byte) 93;
        read[367] = (byte) 94;
        //M48 = 
        read[368] = (byte) 92;
        read[369] = (byte) 93;
        read[370] = (byte) 94;
        read[371] = (byte) 95;
        //M49 = 
        read[372] = (byte) 93;
        read[373] = (byte) 94;
        read[374] = (byte) 95;
        read[375] = (byte) 96;
        //M50 = 
        read[376] = (byte) 94;
        read[377] = (byte) 95;
        read[378] = (byte) 96;
        read[379] = (byte) 97;
        //M51 = 
        read[380] = (byte) 95;
        read[381] = (byte) 96;
        read[382] = (byte) 97;
        read[383] = (byte) 98;
        //M52 = 
        read[384] = (byte) 96;
        read[385] = (byte) 97;
        read[386] = (byte) 98;
        read[387] = (byte) 99;
        //M53 = 
        read[388] = (byte) 97;
        read[389] = (byte) 98;
        read[390] = (byte) 99;
        read[391] = (byte) 100;
        //M54 = 
        read[392] = (byte) 98;
        read[393] = (byte) 99;
        read[394] = (byte) 100;
        read[395] = (byte) 101;
        //M55 = 
        read[396] = (byte) 99;
        read[397] = (byte) 100;
        read[398] = (byte) 101;
        read[399] = (byte) 102;
        //M56 = 
        read[400] = (byte) 100;
        read[401] = (byte) 101;
        read[402] = (byte) 102;
        read[403] = (byte) 103;
        //M57 = 
        read[404] = (byte) 101;
        read[405] = (byte) 102;
        read[406] = (byte) 103;
        read[407] = (byte) 104;
        //M58 = 
        read[408] = (byte) 102;
        read[409] = (byte) 103;
        read[410] = (byte) 104;
        read[411] = (byte) 105;
        //M59 = 
        read[412] = (byte) 103;
        read[413] = (byte) 104;
        read[414] = (byte) 105;
        read[415] = (byte) 106;
        //M60 = 
        read[416] = (byte) 104;
        read[417] = (byte) 105;
        read[418] = (byte) 106;
        read[419] = (byte) 107;
        //M61 = 
        read[420] = (byte) 105;
        read[421] = (byte) 106;
        read[422] = (byte) 107;
        read[423] = (byte) 108;
        //M62 = 
        read[424] = (byte) 106;
        read[425] = (byte) 107;
        read[426] = (byte) 108;
        read[427] = (byte) 109;
        //M63 = 
        read[428] = (byte) 107;
        read[429] = (byte) 108;
        read[430] = (byte) 109;
        read[431] = (byte) 110;
        //Clocks = 
        read[432] = (byte) 111;
        read[433] = (byte) 112;
        read[434] = (byte) 113;
        read[435] = (byte) 114;
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void readAll(){
        list = InstructionUtils.translate(inst);
        assertNotNull(list);
        assertEquals(35, list.size());
        list = InstructionUtils.translate("VVLL 32 33 21");
        assertNull(list);
        list = InstructionUtils.translate("LB 5");
        assertNull(list);
        list = InstructionUtils.translate("VVLL 32 33 21");
        assertNull(list);
    }
    
    @Test
    public void instruction(){
        list = InstructionUtils.translate(inst);
        String line;
        Instructions in;
        for (int i = 0; i < list.size(); i++) {
            line = decodes[i].trim().replace(" ", "");
            in = list.get(i);
            assertEquals(line, in.getBinary());
        }
    }
    
    @Test
    public void binary(){
        list = InstructionUtils.translate(inst);
        for (int i = 0; i < list.size(); i++){
            assertNotNull(InstructionUtils.fromString(decodes[i].replace(" ", "")));
            assertEquals(decodes[i].replace(" ", ""), InstructionUtils.fromString(decodes[i].replace(" ", "")).getBinary());
        }
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), InstructionUtils.fromString(decodes[i].replace(" ", "")));
            assertEquals(list.get(i).getLine().replaceAll(" +", " "), InstructionUtils.fromString(decodes[i].replace(" ", "")).getInstruction());
        }
    }
    
    @Test
    public void bytes(){
        list = InstructionUtils.translate(inst);
        byte[] result = InstructionUtils.getBytes(list);
        for (int i = 0; i < bytes.length; i++) {
            assertEquals(bytes[i], result[i]);
        }
    }
    
    @Test
    public void read(){
        ProcessorsData pro = InstructionUtils.readMemories(read);
        assertNotNull(pro);
        assertNotNull(pro.getLatchEX());
        assertFalse(pro.getLatchEX().isEmpty());
        assertNotNull(pro.getLatchID());
        assertFalse(pro.getLatchID().isEmpty());
        assertNotNull(pro.getLatchIF());
        assertFalse(pro.getLatchIF().isEmpty());
        assertNotNull(pro.getLatchMEM());
        assertFalse(pro.getLatchMEM().isEmpty());
        assertNotNull(pro.getMemory());
        assertFalse(pro.getMemory().isEmpty());
        assertNotNull(pro.getRegisters());
        assertFalse(pro.getRegisters().isEmpty());
        assertEquals(5, pro.getLatchEX().size());
        assertEquals(13, pro.getLatchID().size());
        assertEquals(2, pro.getLatchIF().size());
        assertEquals(4, pro.getLatchMEM().size());
        assertEquals(64, pro.getMemory().size());
        assertEquals(32, pro.getRegisters().size());
        
        assertEquals(50462976, pro.getLatchIF().get(0).getValue());
        assertEquals(67305985, pro.getLatchIF().get(1).getValue());
        
        assertEquals(84148994, pro.getLatchID().get(0).getValue());
        assertEquals(100992003, pro.getLatchID().get(1).getValue());
        assertEquals(117835012, pro.getLatchID().get(2).getValue());
        assertEquals(1, pro.getLatchID().get(3).getValue());
        assertEquals(0, pro.getLatchID().get(4).getValue());
        assertEquals(1, pro.getLatchID().get(5).getValue());
        assertEquals(10, pro.getLatchID().get(6).getValue());
        assertEquals(7, pro.getLatchID().get(7).getValue());
        assertEquals(25, pro.getLatchID().get(8).getValue());
        assertEquals(4, pro.getLatchID().get(9).getValue());
        assertEquals(9, pro.getLatchID().get(10).getValue());
        assertEquals(134678021, pro.getLatchID().get(11).getValue());
        assertEquals(151521030, pro.getLatchID().get(12).getValue());
        
        assertEquals(757869354, pro.getLatchEX().get(0).getValue());
        assertEquals(0, pro.getLatchEX().get(1).getValue());
        assertEquals(1, pro.getLatchEX().get(2).getValue());
        assertEquals(7, pro.getLatchEX().get(3).getValue());
        assertEquals(0, pro.getLatchEX().get(4).getValue());
        
        assertEquals(774712363, pro.getLatchMEM().get(0).getValue());
        assertEquals(1, pro.getLatchMEM().get(1).getValue());
        assertEquals(0, pro.getLatchMEM().get(2).getValue());
        assertEquals(27, pro.getLatchMEM().get(3).getValue());
        
        for (int i = 0; i < pro.getRegisters().size(); i++) {
            assertEquals((168364039 + (i * 16843009)), pro.getRegisters().get(i).getValue());
        }
        
        for (int i = 0; i < pro.getMemory().size(); i++) {
            assertEquals(791555372 + i * 16843009, pro.getMemory().get(i).getValue());
        }
        
        assertEquals(1920036975, pro.getClock().getValue());
    }
}
